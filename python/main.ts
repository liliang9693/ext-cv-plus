
//% color="#bf00bf" iconWidth=50 iconHeight=40
namespace cvPlus{


    //% block="对图片[IMGIN]进行高斯模糊平滑处理，输出为图像[IMGOUT]" blockType="command"
    //% IMGIN.shadow="normal" IMGIN.defl="img"
    //% IMGOUT.shadow="normal" IMGOUT.defl="outImg"
    export function gaussian(parameter: any, block: any) {
        let IMGIN=parameter.IMGIN.code;
        let IMGOUT=parameter.IMGOUT.code;
        Generator.addImport(`import cv2\nimport numpy as np`)
        Generator.addCode(`${IMGOUT}= cv2.GaussianBlur(${IMGIN}, (5, 5), 0)`)
 
    }

    //% block="对图片[IMGIN]进行边缘检测，阈值1[TH1]、阈值2[TH2]，输出边缘二值化图像[IMGOUT]" blockType="command"
    //% IMGIN.shadow="normal" IMGIN.defl="img"
    //% IMGOUT.shadow="normal" IMGOUT.defl="outImg"
    //% TH1.shadow="number" TH1.defl="100"
    //% TH2.shadow="number" TH2.defl="200"
    export function canny(parameter: any, block: any) {
        let IMGIN=parameter.IMGIN.code;
        let IMGOUT=parameter.IMGOUT.code;
        let TH1=parameter.TH1.code;
        let TH2=parameter.TH2.code;
        Generator.addImport(`import cv2\nimport numpy as np`)
        Generator.addCode(`${IMGOUT}=cv2.Canny(${IMGIN},${TH1},${TH2})`)
 
    }
  
    //% block="在图片[IMGIN]中查找轮廓点，[MODE]模式、[METHOD]方法，结果返回到序列[CONTOURS]" blockType="command"
    //% IMGIN.shadow="normal" IMGIN.defl="img"
    //% CONTOURS.shadow="normal" CONTOURS.defl="contours"
    //% MODE.shadow="dropdown" MODE.options="MODE"
    //% METHOD.shadow="dropdown" METHOD.options="METHOD"
    export function contours(parameter: any, block: any) {
        let IMGIN=parameter.IMGIN.code;
        let CONTOURS=parameter.CONTOURS.code;
        let MODE=parameter.MODE.code;
        let METHOD=parameter.METHOD.code;
        Generator.addImport(`import cv2\nimport numpy as np`)
        Generator.addCode(`${CONTOURS}, hierarchy = cv2.findContours(${IMGIN},cv2.${MODE},cv2.${METHOD})`)
 
    }
  
    //% block="在图片[IMG]上绘制轮廓线[CONTOURS] [COLOR] 线宽[THICK]，输出带轮廓图像[IMGOUT]" blockType="command"
    //% IMG.shadow="normal" IMG.defl="img"
    //% IMGOUT.shadow="normal" IMGOUT.defl="outImg"
    //% CONTOURS.shadow="normal" CONTOURS.defl="contours"
    //% COLOR.shadow="colorPalette" 
    //% THICK.shadow="number" THICK.defl="2"
    export function drawContours(parameter: any, block: any) {
  
        let IMG=parameter.IMG.code;
        let IMGOUT=parameter.IMGOUT.code;
        let CONTOURS=parameter.CONTOURS.code;
        let COLOR=parameter.COLOR.code;
        let THICK=parameter.THICK.code;

        var r = 0;
        var g = 0;
        var b = 0;
        try {
            if ( COLOR.length == 8 ) {//分别截取RGB值然后转换为数字值
                r = parseInt(COLOR.substring(2, 4), 16);
                g = parseInt(COLOR.substring(4, 6), 16);
                b = parseInt(COLOR.substring(6, 8), 16);
            }
        } catch(e) {
            return '';
        }

        Generator.addImport(`import cv2\nimport numpy as np`)
        Generator.addCode(`${IMGOUT}=cv2.drawContours(${IMG}, ${CONTOURS}, -1, (${b},${g},${r}), ${THICK})`)
       
 
    }


    //% block="---"
    export function noteSep2() {}

   
    function replaceQuotationMarks(str:string){
            str=str.replace(/"/g, ""); //去除所有引号
            return str
    }


    
}
